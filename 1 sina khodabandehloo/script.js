//const input1 = prompt('Enter values: ');
input1 = '5 100 1';  //for test
const arr = input1.split(' ').map( inp => (parseInt(inp)));
// number of dancers = arr[0];
// cost of 0-suit = arr[1]
// cost of 1-suit = arr[2]

//const input2 = prompt('Enter costs: ');
input2 = '0 1 2 1 2'; //for test
const costs = input2.split(' ').map( inp => (parseInt(inp)));

let ans = 0;
const minval = Math.min(arr[1], arr[2]);
const maxval = Math.max(arr[1], arr[2]);

const centerIndex = Math.ceil(costs.length / 2) - 1;

console.log(centerIndex);

function check(){
    const leftArr = [];
    const rightArr = [];

    // check center
    if (centerIndex % 2 === 0 && centerIndex === 2){
        ans += minval;
    }

    //create the left array
    for (i = 0; i <= centerIndex; i++){
        leftArr.push(costs[i]);
    }
    //create the right array
    for (i = centerIndex + 1; i < costs.length; i++){
        rightArr.push(costs[i]);
    }

    if (leftArr.length != rightArr.length) leftArr.pop();

    console.log(leftArr, '//', rightArr)
}

function pairIndex(ind, arr){
        return arr.length - (ind + 1);
}

document.getElementById('ans').innerHTML = check();