const [n, ...costs] = prompt(
    "Please insert n price0 price1"
)
    .split(" ")
    .map(val => +val);

const dancers = prompt("Please insert dancers dress");



let cost = 0;
let isImpossible = false;
const len = dancers.length;
for (let i = 0; i < parseInt(len / 2); i++) {
    const curr = +dancers[i];
    const mir = +dancers[len - 1 - i];

    if (curr != 2 && mir != 2 && curr != mir) {
        cost = -1;
        isImpossible = true
        break;
    }

    if (curr == 2 && mir != 2) {
        cost += costs[mir];
    }

    if (mir == 2 && curr != 2) {
        cost += costs[curr];
    }

    if (mir == 2 && curr == 2) {
        cost += Math.min.call(null, ...costs) * 2;
    }
}

if (n % 2 != 0 && !isImpossible && dancers[(len - 1) / 2] == 2) {
    cost += Math.min.call(null, ...costs)
}
console.log(cost);
alert (`your cost is ${cost}`)