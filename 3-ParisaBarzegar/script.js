
// ==================== Show red circle nearby menu items ====================
const menuItem = [...document.getElementsByTagName('li')];
const menu = document.getElementById('list-item')

menu.addEventListener('click',(e)=>{
    menuItem.forEach(item => {
        item.classList.remove('active')
    })
    e.target.parentElement.classList.add('active')
})

// ==================== red bookmark ====================
const bookmark = [...document.getElementsByClassName('bookmark')]
// console.log(bookmark)
bookmark.map(item =>{
    item.addEventListener('click', ()=>{
        item.src = 'img/small-bookmark2.svg'
    })
})

// ==================== Show three dots menu ====================
const dotedMenu = [...document.getElementsByClassName('menu-icon')]
const editeMenu = [...document.getElementsByClassName('edite-menu')]

dotedMenu.map((item,index)=>{
    item.addEventListener('click', ()=>{
        editeMenu[index].style.display = 'flex'
    })
})

// ==================== close edite menu ====================
var main = document.getElementById('main')
main.addEventListener('click', (event)=>{
        // alert('salam')
        console.log(event.target.classList);
        
        editeMenu.forEach(item =>{
            if(!event.target.classList.contains('menu-icon'))
            if(item.style.display!= 'none')
            item.style.display = 'none'
        })
})